/**
 * Copyright (c) 2014, Johan Vos, LodgON All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met: * Redistributions of source
 * code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution. * Neither the name of LodgON, the website
 * lodgon.com, nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * LODGON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.jww.openmapfx.desktop;

import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import com.jww.openmapfx.core.DefaultBaseMapProvider;
import com.jww.openmapfx.core.LayeredMap;
import com.jww.openmapfx.core.LicenceLayer;
import com.jww.openmapfx.core.PositionLayer;
import com.jww.openmapfx.core.TileProvider;

/**
 * 
 * @author Johan Vos
 * @author Justin Webb
 */
public class MainApp extends Application {

    private LayeredMap map;
    private TileProvider[] tileProviders;
    private SimpleProviderPicker spp;
    private LicenceLayer licenceLayer;

    @Override
    public void start(Stage stage) throws Exception {
	Canvas canvas1 = new Canvas(50,50);
	Canvas canvas2 = new Canvas(50,50);
	GraphicsContext gc1 = canvas1.getGraphicsContext2D();
	GraphicsContext gc2 = canvas2.getGraphicsContext2D();
	gc1.strokeLine(0, 25, 50, 25);
	gc2.strokeLine(0, 25, 50, 25);
	
	DefaultBaseMapProvider provider = new DefaultBaseMapProvider();

	spp = new SimpleProviderPicker(provider);

	map = new LayeredMap(provider);

	BorderPane cbp = new BorderPane();
	cbp.setCenter(map);

	Rectangle clip = new Rectangle(700, 600);
	cbp.setClip(clip);
	clip.heightProperty().bind(cbp.heightProperty());
	clip.widthProperty().bind(cbp.widthProperty());

	BorderPane bp = new BorderPane();
//	bp.setTop(spp);
	bp.setCenter(cbp);
	BorderPane.setAlignment(canvas1, Pos.CENTER);
	bp.setLeft(canvas1);
	BorderPane.setAlignment(canvas2, Pos.CENTER);
	bp.setRight(canvas2);

	Scene scene = new Scene(bp, 800, 650);
	stage.setScene(scene);
	stage.show();
	map.setZoom(2);
	map.setCenter(0, 0);
	showMyLocation();

	licenceLayer = new LicenceLayer(provider);
	map.getLayers().add(licenceLayer);
	
//        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
//        
//        Scene scene = new Scene(root);
//        scene.getStylesheets().add("/styles/Styles.css");
//        
//        stage.setTitle("JavaFX and Maven");
//        stage.setScene(scene);
//        stage.show();
    }

    private void showMyLocation() {
	URL im = this.getClass().getResource("/image/mylocation.png");
	Image image = new Image(im.toString());
	PositionLayer positionLayer = new PositionLayer(image);
	map.getLayers().add(positionLayer);
	positionLayer.updatePosition(51.2, 4.2);
	map.centerLatitudeProperty().addListener(new ChangeListener<Number>() {
	    @Override
	    public void changed(ObservableValue<? extends Number> ob, Number o, Number n) {
		System.out.println("center of map: lat = " + map.centerLatitudeProperty().get() + ", lon = " + map.centerLongitudeProperty().get());
	    }
	});
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
