package com.jww.openmapfx.providers;

import com.jww.openmapfx.core.TileProvider;
import com.jww.openmapfx.core.TileType;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Justin
 */
public class USGSTileProvider implements TileProvider {
    private static final String providerName = "USGS";
    
    private static final List<TileType> tileTypes = new LinkedList<>();;
    static {
        tileTypes.add(new TileType("Topo", "http://basemap.nationalmap.gov/arcgis/rest/services/USGSTopo/MapServer/tile/", "USGS"));
        tileTypes.add(new TileType("Imagery Topo", "http://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryTopo/MapServer/tile/", "USGS"));
	tileTypes.add(new TileType("Imagery", "http://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/", "USGS"));
	tileTypes.add(new TileType("Hydro NHD", "http://basemap.nationalmap.gov/arcgis/rest/services/USGSHydroNHD/MapServer/tile/", "USGS"));
	tileTypes.add(new TileType("Shaded Relief", "http://basemap.nationalmap.gov/arcgis/rest/services/USGSShadedReliefOnly/MapServer/tile/", "USGS"));
    }
    
    public USGSTileProvider() {
    }
    
    @Override
    public String getProviderName() {
        return providerName;
    }

    @Override
    public List<TileType> getTileTypes() {
        return tileTypes;
    }
    
    @Override
    public TileType getDefaultType() {
        return tileTypes.get(0);
    }
    
    @Override
    public String getAttributionNotice() {
        //return "Tiles Courtesy of <a href=\"http://www.mapquest.com/\" target=\"_blank\">MapQuest</a> <img src=\"http://developer.mapquest.com/content/osm/mq_logo.png\">";
        return "Tiles Courtesy of USGS (http://www.usgs.gov/)";
    }
    
    @Override
    public String toString() {
        return getProviderName();
    }
}
